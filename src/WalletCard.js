import React, {useState} from 'react'
import {ethers} from 'ethers'
import { Row, Col, Card, Button } from 'react-bootstrap'

const WalletCard = () => {
    const [name, setName] = useState('');
    const [jayPegs, setJaypegs] = useState(null);
  
    const p1 = 'https://api.nftport.xyz/v0/accounts/';
    const p3 = '?chain=ethereum';
    const fullUrl = p1 + name + p3;
  
    function fetchNFT() {
      fetch(fullUrl, {
        headers: {
          Authorization: '169fe2e8-5b9d-4104-9891-98b293dacbeb'
        }
      })
      .then(resp => resp.json())
      .then(data => {setJaypegs(data.nfts);console.log(data.nfts);})
    }

	const [errorMessage, setErrorMessage] = useState(null);
	const [setDefaultAccount] = useState(null);
	const [userBalance, setUserBalance] = useState(null);
	const [connButtonText, setConnButtonText] = useState('Connect your Wallet');

	const connectWalletHandler = () => {
		if (window.ethereum && window.ethereum.isMetaMask) {
			console.log('MetaMask Here!');

			window.ethereum.request({ method: 'eth_requestAccounts'})
			.then(result => {
				accountChangedHandler(result[0]);
				setConnButtonText((result[0]) + ' ✓');
				getAccountBalance(result[0]);
                setName(result[0]);
                console.log('MM addr = ' + result[0]);
			})
			.catch(error => {
				setErrorMessage(error.message);
			});

		} else {
			console.log('Need to install MetaMask');
			setErrorMessage('Please install MetaMask browser extension to interact');
		}
	}

	// update account, will cause component re-render
	const accountChangedHandler = (newAccount) => {
		setDefaultAccount(newAccount);
		getAccountBalance(newAccount.toString());
        setName(newAccount);
        setConnButtonText(newAccount);
        console.log('MM new addr = ' + newAccount);
	}

	const getAccountBalance = (account) => {
		window.ethereum.request({method: 'eth_getBalance', params: [account, 'latest']})
		.then(balance => {
			setUserBalance(ethers.utils.formatEther(balance));
		})
		.catch(error => {
			setErrorMessage(error.message);
		});
	};

	const chainChangedHandler = () => {
		// reload the page to avoid any errors with chain change mid use of application
		window.location.reload();
	}


	// listen for account changes
	window.ethereum.on('accountsChanged', accountChangedHandler);

	window.ethereum.on('chainChanged', chainChangedHandler);
	
	return (
		<div className='walletCard'>
            <Row>
				<Col xs={12} md={6} lg={6} className='paste'>
					<div className='field'>
						<h3>Connect with MetaMask</h3>
                    </div>
                    <div className='d-grid'>
            			<Button onClick={connectWalletHandler} variant='outline-primary' size='lg'>{connButtonText}</Button>
                        <br />
                        <Button className='fetch' variant='outline-primary' size='lg' onClick={fetchNFT}>Fetch NFTs</Button>  
                    </div>
                </Col>
                <Col>
                    <div className='balanceDisplay'>
				        <p>Balance: {userBalance}</p>
		        	</div>
			        {errorMessage}
                </Col>
            </Row>
            <br />
            <Row>
				{jayPegs && jayPegs.map((jayPeg, index)  => (
				jayPeg.file_url ?
					<Card style={{ width: '16rem', marginRight: '8px' }} key={index}>
						<Card.Img variant='top' src={jayPeg.file_url} />
						<Card.Body>
						<Card.Title>{jayPeg.name}</Card.Title>
						<Card.Text>{jayPeg.description}</Card.Text>
						</Card.Body>
						<Button variant='primary' size='lg' style={{width:'100%', marginBottom:'6px', marginTop:'6px'}}>
						Add to Creator
						</Button>
					</Card>
				: null
				))}
			</Row>	
		</div>
	);
}

export default WalletCard;