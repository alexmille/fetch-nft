import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col, Accordion, Table } from 'react-bootstrap';
import WalletCard from './WalletCard';
import PasteAddress from './PasteAddress';

function App() {

  return (
    <div className='App'>
      <Container>
        <br />
        <Row>
          <Col>
            <h1>Print your NFT !</h1>
          </Col>
        </Row>
        <br />
        <Row className='how'>
          <Col xs={6} md={6} lg={6}>
            <h3>How does it work ?</h3>
            <ul style={{textAlign: 'left', listStyle: 'none'}}>
              <li>NFT data provided by <a href='nftport.xyz'>NFTport</a></li>
              <li>Connect with MetaMask works with npm package <a href='https://www.npmjs.com/package/ethers'>Ethers</a></li>
              <li></li>
            </ul>
          </Col>
          <Col xs={6} md={6} lg={6}>
            <h3>A few addresses to play with</h3>
            <Table striped bordered hover size='sm'>
              <tbody>
                <tr>
                  <td>Cozomo de Medici</td>
                  <td>0xce90a7949bb78892f159f428d0dc23a8e3584d75</td>
                </tr>
                <tr>
                  <td>Trevor Jones</td>
                  <td>0x6c6e93874216112ef12a0d04e2679ecc6c3625cc</td>
                </tr>
                <tr>
                  <td>Alotta Money</td>
                  <td>0xbd9b7373aac15d9a93c810df3999343f4fe1ed88</td>
                </tr>
                <tr>
                  <td>DC Investor</td>
                  <td>0x59a5493513ba2378ed57ae5ecfb8a027e9d80365</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
        <br />
        <Row>
          <Col>
            <Accordion>
              <Accordion.Item eventKey='0'>
                <Accordion.Header>Paste an Ethereum Address</Accordion.Header>
                <Accordion.Body>
                  <PasteAddress />
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey='1'>
                <Accordion.Header>
                  Connect to Metamask
                  <img 
                    src={process.env.PUBLIC_URL+'metamask.svg'} 
                    style={{width: '32px', marginLeft: '20px'}} 
                  />
                </Accordion.Header>
                <Accordion.Body>
                <WalletCard />
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </Col>
        </Row>
      </Container>
      <br />
    </div>
  );
}

export default App;
