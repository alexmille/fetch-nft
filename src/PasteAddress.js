import React, { useState } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Row, Col, Card, Button } from 'react-bootstrap';

const PasteAddress = () => {
    const [name, setName] = useState('');
    const [jayPegs, setJaypegs] = useState(null);
  
    const p1 = 'https://api.nftport.xyz/v0/accounts/';
    const p3 = '?chain=ethereum';
    const fullUrl = p1 + name + p3;
  
    function fetchNFT() {
      fetch(fullUrl, {
        headers: {
          Authorization: '169fe2e8-5b9d-4104-9891-98b293dacbeb'
        }
      })
      .then(resp => resp.json())
      .then(data => {setJaypegs(data.nfts);console.log(data.nfts);})
    }
  
    return(
		<div>
			<Row>
				<Col xs={12} md={6} lg={6} className='paste'>
					<div className='field'>
						<h3>Paste your Ethereum address</h3>
						<input 
							className='input' 
							type='text' 
							placeholder='0x1a2b3c4d5e6a7f...' 
							value={name}
							style={{width:'400px'}}
							onChange={e => setName(e.target.value)}>
						</input>
					</div>
					<br />
					<div className='d-grid gap-2'>
						<Button className='fetch' variant='outline-primary' size='lg' onClick={fetchNFT}>Fetch NFTs</Button>  
					</div>
				</Col>
	        </Row>
			<br />
    	    <Row>
				{jayPegs && jayPegs.map((jayPeg, index)  => (
				jayPeg.file_url ?
					<Card style={{ width: '16rem', marginRight: '8px' }} key={index}>
						<Card.Img variant='top' src={jayPeg.file_url} />
						<Card.Body>
						<Card.Title>{jayPeg.name}</Card.Title>
						<Card.Text>{jayPeg.description}</Card.Text>
						</Card.Body>
						<Button variant='primary' size='lg' style={{width:'100%', marginBottom:'6px', marginTop:'6px'}}>
						Add to Creator
						</Button>
					</Card>
				: null
				))}
			</Row>	
		</div>
    )
}

export default PasteAddress;